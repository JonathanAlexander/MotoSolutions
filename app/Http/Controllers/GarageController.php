<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Garage;

class GarageController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = \Auth::user()->id;
        $garage = Garage::where('user_id', $id)->get();
        return view('garage.index' ,compact('garage'))
            ->with('i', (request()->input('page', 1) -1) * 5);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = \Auth::user()->id;
        return view('garage.create', ['id' => $id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'year' => 'required',
            'make' => 'required',
            'model' => 'required',
        ]);



        $garage = Garage::create($request->all());

        $this->imageUpload($request, $garage);



        return redirect()->route('garage.show', compact('garage'))
            ->with('success','Garage record created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $garage = Garage::find($id);
        return view('garage.show', compact('garage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $garage = Garage::find($id);
        return view('garage.edit',compact('garage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'year' => 'required',
            'make' => 'required',
            'model' => 'required',
        ]);
        Garage::find($id)->update($request->all());
        return redirect()->route('garage.index')
            ->with('success','Garage record created successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){

        $garage = Garage::find($id);

        $garage->suspensions()->delete();
        $garage->carbjettings()->delete();
        $garage->drives()->delete();
        $garage->journals()->delete();
        $garage->delete();


        return redirect()->route('garage.index')
            ->with('success','Garage record deleted successfully');
    }


    public function imageUpload(Request $request, Garage $garage)

    {


        $this->validate($request, [

            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:4096',

        ]);

        $image = $request->file('image');



        $ext = $image->getClientOriginalExtension();

        $image->storeAs('public/images/' . auth()->id() . '/' . $garage->id, "image.{$ext}");


        return back()->with('success', 'Image Upload successful');
    }

}
