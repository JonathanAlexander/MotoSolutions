<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class test extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date'
    ];

    protected $hidden = ['id'];


    /**
     * Get the garage record associated with the drive item.
     */
    public function garage()
    {
        return $this->belongsTo('App\Garage', 'garage_id');
    }
}
