<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Suspension extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suspensions', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('garage_id')->unsigned()->nullable();
            $table->foreign('garage_id')->references('id')->on('garages');
            $table->string('fork_compression')->nullable();
            $table->string('fork_rebound')->nullable();
            $table->string('fork_height')->nullable();
            $table->string('fork_oil_level')->nullable();
            $table->string('fork_spring')->nullable();
            $table->string('shock_compression')->nullable();
            $table->string('shock_rebound')->nullable();
            $table->string('shock_spring')->nullable();
            $table->string('shock_oil_level')->nullable();
            $table->string('shock_nitrogen')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suspensions');
    }
}
