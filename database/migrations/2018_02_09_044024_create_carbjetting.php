<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarbjetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carb_jettings', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('garage_id')->unsigned()->nullable();
            $table->foreign('garage_id')->references('id')->on('garages');
            $table->string('main')->nullable();
            $table->string('pilot')->nullable();
            $table->string('clip_position')->nullable();
            $table->string('needle')->nullable();
            $table->string('float_height')->nullable();
            $table->string('idle_screw')->nullable();
            $table->string('airfuel_screw')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carb_jettings');
    }
}
