<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Drive extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drives', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('garage_id')->unsigned()->nullable();
            $table->foreign('garage_id')->references('id')->on('garages');
            $table->string('chain')->nullable();
            $table->string('front_sprocket')->nullable();
            $table->string('rear_sprocket')->nullable();
            $table->string('links')->nullable();
            $table->string('slack')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drives');
    }
}
