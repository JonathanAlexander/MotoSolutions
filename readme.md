Welcome To MotoSolution WebApp

This a php CRUD learning project that I have created using Laravel framework
In depth documentation can be found here about this project
https://www.digitalocean.com/community/tutorials/how-to-set-up-laravel-nginx-and-mysql-with-docker-compose

Quick start guide:

Ubuntu:

Install docker and docker compose:

 curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
 
 sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu
 
 sudo apt install docker-ce
 
 sudo systemctl enable docker
 
 docker -v
 
 sudo usermod -aG docker $(whoami)
 
 sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
 
 sudo chmod +x /usr/local/bin/docker-compose
 
 sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose





  
