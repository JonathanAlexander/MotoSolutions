@extends('layouts.app')


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 margin-tb">

                <h2><strong>{{ $garage->year}} {{ $garage->make}} {{ $garage->model}}</strong>

                    <a class="btn btn-primary" href="{{ route('garage.index') }}" style="float: right"> Back</a>

                </h2>
            </div>
        </div>

        <table class="table table-striped table-bordered ">
            <img src="{{Storage::disk()->url('images' . '/' . auth()->id() .'/' . $garage->id . '/' . 'image.jpg')}}" height="40%" width="40%">
        </table>


        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif


        <table class="table table-bordered">

            @if(count($garage->journal) > 0)
                @include('journal.show')
            @else
                <a class="btn btn-success" href="{{ route('journal.create', ['id' => $garage->id]) }}"> Create Journal
                    Entry</a>
            @endif

            @if(count($garage->drive) > 0)
                @include('drive.show')
            @else
                <a class="btn btn-success" href="{{ route('drive.create', ['id' => $garage->id]) }}"> Create Drive</a>
            @endif

            @if(count($garage->carbjetting) > 0)
                @include('carbjetting.show')
            @else
                <a class="btn btn-success" href="{{ route('carbjetting.create', ['id' => $garage->id]) }}"> Create Carb
                    Jetting Specs</a>
            @endif

            @if(count($garage->suspension) > 0)
                @include('suspension.show')
            @else
                <a class="btn btn-success" href="{{ route('suspension.create', ['id' => $garage->id]) }}"> Create
                    Suspension Settings</a>
            @endif


        </table>


        {{{$garage}}}

        {{--  @include('drive.show')

          @include('carbjetting.index')

          @include('suspension.index')

          @include('journal.index')
      --}}
    </div>
@endsection