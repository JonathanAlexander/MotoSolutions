<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {!! Form::hidden('garage_id', request('id'), null) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Pilot</strong>
            {!! Form::text('pilot', null, array('placeholder' => 'Pilot','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Main Jet</strong>
            {!! Form::text('main', null, array('placeholder' => 'Main Jet','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Clip Position</strong>
            {!! Form::text('clip_position', null, array('placeholder' => 'Clip Position','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Needle</strong>
            {!! Form::text('needle', null, array('placeholder' => 'Needle','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Float Height</strong>
            {!! Form::text('float_height', null, array('placeholder' => 'Float Height','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Idle Screw</strong>
            {!! Form::text('idle_screw', null, array('placeholder' => 'Idle Screw','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Air Fuel Screw</strong>
            {!! Form::text('airfuel_screw', null, array('placeholder' => 'Air Fuel Screw','class' => 'form-control')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>