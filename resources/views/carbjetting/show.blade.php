<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2><strong>Carburetor Settings</strong></h2>
        </div>

        <div style="float: right">
            <a class="btn btn-primary" href="{{ route('drive.edit',$garage->id) }}">Edit</a>

            {!! Form::open(['method' => 'DELETE','route' => ['drive.destroy', $garage->id],'style'=>'float:right']) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </div>

    </div>
</div>

@foreach ($garage->carbjetting as $carbjetting)

<table class="table table-bordered">
        <tr>
            <th>Pilot</th>
            <th>Main</th>
            <th>Needle</th>
            <th>Clip Position</th>
            <th>Float Height</th>
            <th>Idle Screw</th>
            <th>Air Fuel Screw</th>
        </tr>

        <tr>
            <td>{{ $carbjetting->pilot}}</td>
            <td>{{ $carbjetting->main}}</td>
            <td>{{ $carbjetting->needle}}</td>
            <td>{{ $carbjetting->clip_position}}</td>
            <td>{{ $carbjetting->float_height}}</td>
            <td>{{ $carbjetting->idle_screw}}</td>
            <td>{{ $carbjetting->airfuel_screw}}</td>
        </tr>
    </table>

@endforeach

