<div class="row">
    <div class="col-lg-12 margin-tb">
        <h2><strong>Ride Journal</strong>
            <div style="float: right">
                <a class="btn btn-primary" href="{{ route('journal.edit',$garage->id) }}">Edit</a>
                {!! Form::open(['method' => 'DELETE','route' => ['journal.destroy', $garage->id],'style'=>'float:right']) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </div>
        </h2>
    </div>
</div>

@foreach ($garage->journal as $journal)

    <table class="table table-bordered">
        <tr>
            <th>Date</th>
            <th>Location</th>
            <th>Seat Time</th>
            <th>Notes</th>
        </tr>

        <tr>
            <td>{{ $journal->date}}</td>
            <td>{{ $journal->location}}</td>
            <td>{{ $journal->seat_time}}</td>
            <td>{{ $journal->notes}}</td>
        </tr>
    </table>

@endforeach