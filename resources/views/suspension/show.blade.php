<div class="row">
    <div class="col-lg-12 margin-tb">
        <h2><strong>Suspension</strong>
            <div style="float: right">
                <a class="btn btn-primary" href="{{ route('suspension.edit',$garage->id) }}">Edit</a>
                {!! Form::open(['method' => 'DELETE','route' => ['suspension.destroy', $garage->id],'style'=>'float:right']) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </div>
        </h2>
    </div>


</div>

@foreach ($garage->suspension as $suspension)

    <table class="table table-bordered">
        <tr>
            <th>Fork Compression</th>
            <th>Fork Rebound</th>
            <th>Fork Height</th>
            <th>Fork Oil Level</th>
            <th>Fork Spring</th>

        </tr>

        <tr>
            <td>{{ $suspension->fork_compression}}</td>
            <td>{{ $suspension->fork_rebound}}</td>
            <td>{{ $suspension->fork_height}}</td>
            <td>{{ $suspension->fork_oil_level}}</td>
            <td>{{ $suspension->fork_sprint}}</td>
        </tr>

    </table>

    <table class="table table-bordered">
        <tr>
            <th>Shock Compression</th>
            <th>Shock Rebound</th>
            <th>Shock Preload</th>
            <th>Shock Spring</th>
            <th>Shock Oil Level</th>
            <th>Shock Nitrogen Amount</th>
        </tr>


        <tr>
            <td>{{ $suspension->shock_compression}}</td>
            <td>{{ $suspension->shock_rebound}}</td>
            <td>{{ $suspension->shock_preload}}</td>
            <td>{{ $suspension->shock_spring}}</td>
            <td>{{ $suspension->shock_oil_level}}</td>
            <td>{{ $suspension->shock_nitrogen}}</td>
        </tr>
    </table>
@endforeach
